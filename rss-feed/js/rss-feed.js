var current_url = "https://www.channelnewsasia.com/rssfeeds/8395986";

loadNewsLink(current_url);
document.getElementById("header").textContent = "Lastest News";

function loadNewsLink(news_url)
{
	$.ajax({
		type: "GET",
		url: news_url,
		success: function(data) {
			var $XML = $(data);
			var html = document.createElement('div');
			$XML.find("item").each(function() {
				var $this = $(this),
					item = {
						title:       $this.find("title").text(),
						link:        $this.find("link").text(),
						description: $this.find("description").text(),
						pubDate:     $this.find("pubDate").text(),
						author:      $this.find("author").text(),
						thumbnail:   $this.find("thumbnail").text()
					};
				html.appendChild(createNewsDiv(item));
			});
			$('.outer-wrapper').html(html);
	   }
	});
}

// Change Theme Button
document.getElementById("theme").addEventListener('click', (e) => {
	var body = document.getElementById('body');
	if (body.className != 'theme-dark') {
		body.setAttribute('style', 'background: #222; color: #fff;');
		body.className = 'theme-dark';
	} else {
		body.className = '';
		body.setAttribute('style', 'background: #fff; color: #000;');
	}
});

// Watch ISS button
document.getElementById("iss").addEventListener('click', (e) => {
document.getElementById("header").textContent = "ISS @ NASA";
	$('.outer-wrapper').html('<iframe width="100%" height="400px" src="http://www.ustream.tv/embed/9408562?html5ui" scrolling="no" allowfullscreen webkitallowfullscreen frameborder="0" style="border: 0 none transparent;"></iframe>');
});


// Reload button
document.getElementById("reload").addEventListener('click', (e) => {
	loadNewsLink(current_url);
});

// Latest News
document.getElementById("latest").addEventListener('click', (e) => {
	document.getElementById("header").textContent = "Lastest News";
	current_url = "https://www.channelnewsasia.com/rssfeeds/8395986";
	loadNewsLink(current_url);
});

// Asia Pacific News
document.getElementById("asia").addEventListener('click', (e) => {
	document.getElementById("header").textContent = "Asia Pacific News";
	current_url = "https://www.channelnewsasia.com/rssfeeds/8395744";
	loadNewsLink(current_url);
});

// Business News
document.getElementById("business").addEventListener('click', (e) => {
	document.getElementById("header").textContent = "Business News";
	current_url = "https://www.channelnewsasia.com/rssfeeds/8395954";
	loadNewsLink(current_url);
});

// Singapore News
document.getElementById("singapore").addEventListener('click', (e) => {
	document.getElementById("header").textContent = "Singapore News";
	current_url = "https://www.channelnewsasia.com/rssfeeds/8396082";
	loadNewsLink(current_url);
});

// Sports News
document.getElementById("sports").addEventListener('click', (e) => {
	document.getElementById("header").textContent = "Sports News";
	current_url = "https://www.channelnewsasia.com/rssfeeds/8395838";
	loadNewsLink(current_url);
});
// Global News
document.getElementById("global").addEventListener('click', (e) => {
	document.getElementById("header").textContent = "Global News";
	current_url = "https://www.channelnewsasia.com/rssfeeds/8395884";
	loadNewsLink(current_url);
});

function createNewsDiv(item)
{

	var newsdiv = document.createElement('div');
	var title = document.createElement('div');
	var link = document.createElement('a');

	title.setAttribute('class', 'title');
	title.textContent = item.title;

	link.appendChild(title);
	link.setAttribute('href', item.link);
	link.setAttribute('class', 'title-link');

	var description = document.createElement('div');
	description.setAttribute('class', 'description');
	description.textContent = item.description;

	var pubDate = document.createElement('div');
	pubDate.setAttribute('class', 'publish-date');
	pubDate.textContent = item.pubDate;

	var author = document.createElement('div');
	author.setAttribute('class', 'author');
	author.textContent = item.author;

	var thumb = document.createElement('img');
	thumb.setAttribute('src', item.thumbnail);
	thumb.setAttribute('class', 'news-img');

	var clearfix = document.createElement('div');
	clearfix.setAttribute('class', 'clearfix');

	newsdiv.setAttribute('class', 'news-div');
	newsdiv.appendChild(thumb);
	newsdiv.appendChild(link);
	newsdiv.appendChild(description);
	newsdiv.appendChild(pubDate);
	newsdiv.appendChild(clearfix);
	newsdiv.appendChild(author);

	return newsdiv;
}
